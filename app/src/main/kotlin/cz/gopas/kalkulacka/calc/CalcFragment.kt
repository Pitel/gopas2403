package cz.gopas.kalkulacka.calc

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import cz.gopas.kalkulacka.R
import cz.gopas.kalkulacka.databinding.FragmentCalcBinding
import cz.gopas.kalkulacka.history.HistoryEntity
import cz.gopas.kalkulacka.history.HistoryViewModel

class CalcFragment : Fragment() {
    private var binding: FragmentCalcBinding? = null
    private val viewModel: CalcViewModel by viewModels()
    private val historyViewModel: HistoryViewModel by viewModels(ownerProducer = { requireActivity() })

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        FragmentCalcBinding.inflate(inflater, container, false)
            .also { binding = it }
            .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding?.run {
            calc.setOnClickListener { calc() }
            share.setOnClickListener { share() }
            ans.setOnClickListener { a.setText("${viewModel.ans}") }
            viewModel.result.observe(viewLifecycleOwner) {
                if (it != null) {
                    res.text = getString(R.string.result, it)
                    historyViewModel.db.add(HistoryEntity(it))
                }
            }
            historyViewModel.selected.observe(viewLifecycleOwner) {
                if (it != null) {
                    b.setText("$it")
                    historyViewModel.selected.value = null
                }
            }
            history.setOnClickListener { findNavController().navigate(CalcFragmentDirections.actionHistory()) }
        }
    }

    private fun calc() {
        Log.d(TAG, "Calc!")
        val a = "${binding!!.a.text}".toFloatOrNull() ?: Float.NaN
        val b = "${binding!!.b.text}".toFloatOrNull() ?: Float.NaN
        val op = binding!!.ops.checkedRadioButtonId
        if (op == R.id.div && b == 0f) {
            findNavController().navigate(CalcFragmentDirections.actionZero())
        } else {
            viewModel.calc(a, b, op)
        }
    }

    private fun share() {
        startActivity(
            Intent(Intent.ACTION_SEND)
                .putExtra(Intent.EXTRA_TEXT, binding!!.res.text)
                .setType("text/plain")
        )
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    private companion object {
        private val TAG = CalcFragment::class.simpleName
    }
}
