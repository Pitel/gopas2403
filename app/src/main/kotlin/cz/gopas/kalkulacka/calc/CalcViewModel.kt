package cz.gopas.kalkulacka.calc

import android.app.Application
import android.util.Log
import androidx.annotation.IdRes
import androidx.core.content.edit
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import cz.gopas.kalkulacka.R
import kotlin.concurrent.thread
import kotlin.time.Duration.Companion.seconds

class CalcViewModel(app: Application) : AndroidViewModel(app) {
    private val _result = MutableLiveData<Float?>(null)
    val result: LiveData<Float?> = _result

    private val prefs = PreferenceManager.getDefaultSharedPreferences(app)

    var ans: Float
        get() = prefs.getFloat(ANS_KEY, Float.NaN)
        private set(value) = prefs.edit { putFloat(ANS_KEY, value) }

    fun calc(a: Float, b: Float, @IdRes op: Int) {
        thread {
            Log.d(TAG, "Calc!")
            Thread.sleep(1.seconds.inWholeMilliseconds)
            _result.postValue(
                when (op) {
                    R.id.add -> a + b
                    R.id.sub -> a - b
                    R.id.mul -> a * b
                    R.id.div -> a / b
                    else -> Float.NaN
                }.also {
                    ans = it
                }
            )
        }
    }

    private companion object {
        private val TAG = CalcViewModel::class.simpleName
        private const val ANS_KEY = "ans"
    }
}