package cz.gopas.kalkulacka.history

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.room.Room

class HistoryViewModel(app: Application) : AndroidViewModel(app) {
    val selected = MutableLiveData<Float?>(null)

    val db = Room.databaseBuilder(app, HistoryDatabase::class.java, "history")
        .allowMainThreadQueries()
        .build()
        .historyDao
}