package cz.gopas.kalkulacka.history

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder

class HistoryAdapter(
    val onClick: (Float) -> Unit
) : ListAdapter<HistoryEntity, HistoryAdapter.HistoryViewHolder>(DIFFER) {

    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int) = getItem(position).id

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = HistoryViewHolder(
        LayoutInflater.from(parent.context).inflate(android.R.layout.simple_list_item_1, parent, false) as TextView
    ).also { Log.d(TAG, "Create") }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        Log.v(TAG, "Bind $position")
        holder.v.apply {
            val item = getItem(position)
            text = "${item.value}"
            setOnClickListener { onClick(item.value) }
        }
    }

    inner class HistoryViewHolder(val v: TextView) : ViewHolder(v)

    private companion object {
        private val TAG = HistoryAdapter::class.simpleName
        private val DIFFER = object : DiffUtil.ItemCallback<HistoryEntity>() {
            override fun areItemsTheSame(oldItem: HistoryEntity, newItem: HistoryEntity) = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: HistoryEntity, newItem: HistoryEntity) = oldItem == newItem
        }
    }
}
