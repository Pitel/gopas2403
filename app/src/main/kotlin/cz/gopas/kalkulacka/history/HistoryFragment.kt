package cz.gopas.kalkulacka.history

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import cz.gopas.kalkulacka.R

class HistoryFragment : Fragment(R.layout.fragment_history) {

    private val viewModel: HistoryViewModel by viewModels(ownerProducer = { requireActivity() })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val historyAdapter = HistoryAdapter {
            viewModel.selected.value = it
            findNavController().popBackStack()
        }
        (view as RecyclerView).apply {
            adapter = historyAdapter
            setHasFixedSize(true)
        }

        viewModel.db.getAll().observe(viewLifecycleOwner) {
            historyAdapter.submitList(it)
        }
    }
}
