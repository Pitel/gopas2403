package cz.gopas.kalkulacka.history

import androidx.annotation.Keep
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface HistoryDao {

    @Insert
    fun add(vararg entity: HistoryEntity)

    @Query("SELECT * FROM history")
    fun getAll(): LiveData<List<HistoryEntity>>
}