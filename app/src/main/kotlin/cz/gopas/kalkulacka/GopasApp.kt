package cz.gopas.kalkulacka

import android.app.Application
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.google.android.material.color.DynamicColors

class GopasApp : Application() {

    init {
        FragmentManager.enableDebugLogging(BuildConfig.DEBUG)
    }

    override fun onCreate() {
        super.onCreate()
        Toast.makeText(this, "App create", Toast.LENGTH_SHORT).show()
        DynamicColors.applyToActivitiesIfAvailable(this)
    }
}