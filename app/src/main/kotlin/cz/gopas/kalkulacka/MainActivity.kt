package cz.gopas.kalkulacka

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import cz.gopas.kalkulacka.calc.CalcFragmentDirections
import cz.gopas.kalkulacka.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
//    private val a: TextInputEditText by lazy { findViewById(R.id.a) }
//    private val b by lazy { findViewById<TextInputEditText>(R.id.b) }
//    private val ops: RadioGroup by lazy { findViewById(R.id.ops) }
//    private val res: MaterialTextView by lazy { findViewById(R.id.res) }

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        if (BuildConfig.DEBUG) {
            Toast.makeText(this, "Create", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "Create")
        }
//        if (savedInstanceState == null) {
//            supportFragmentManager.commit {
//                add(R.id.container, CalcFragment())
//            }
//        }
        Log.d(TAG, "${intent.dataString}")

        val navController = binding.container.getFragment<NavHostFragment>().navController
        setupActionBarWithNavController(navController, AppBarConfiguration(navController.graph))
    }

    override fun onSupportNavigateUp() = findNavController(R.id.container).navigateUp() || super.onSupportNavigateUp()

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Resume")
    }

    override fun onPause() {
        Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Pause")
        super.onPause()
    }

    override fun onStop() {
        Toast.makeText(this, "Stop", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "Stop")
        super.onStop()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.about -> {
            findNavController(R.id.container).navigate(CalcFragmentDirections.actionAbout("Honza Kaláb"))
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

//    override fun onSaveInstanceState(outState: Bundle) {
//        super.onSaveInstanceState(outState)
//        outState.putCharSequence(RES_KEY, res.text)
//    }
//
//    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
//        super.onRestoreInstanceState(savedInstanceState)
//        res.text = savedInstanceState.getCharSequence(RES_KEY)
//    }

    private companion object {
        private val TAG = MainActivity::class.simpleName
//        private const val RES_KEY = "res"
    }
}
